<?php

    include 'vendor/autoload.php';

    use ElephantIO\Client;
    use ElephantIO\Engine\SocketIO\Version1X;

    $version = new Version1X("http://localhost:1337");
    $client  = new Client($version);

    if(isset($_POST['submit'])) {

        $form_data = array("name"=>$_POST['name'], "password"=>$_POST['pwd']);

        $client->initialize();
        $client->emit("new_order", $form_data);
        $client->close();

    }

?>

<!DOCTYPE html>
<html>
<title>Node.js Test</title>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.min.js"></script>-->
</head>

<body>
    <div class="container">
  <h2>Horizontal form</h2>
  <form class="form-horizontal" method="POST" action="#">
    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-10">
        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
      </div>
    </div>
    
    <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Password:</label>
      <div class="col-sm-10">          
        <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
      </div>
    </div>
    
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <input type="submit" class="btn btn-primary" value="Submit" name="submit">
      </div>
    </div>
  </form>
</div>
</body>
</html>